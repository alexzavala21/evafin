import React, { useState, createContext } from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";

import Container from "./components/Container";
import Login from "./containers/Login";
import ValidarLogeo from "./containers/Utilidades/ValidarLogeo";
import ProtectedRouter from "./containers/Utilidades/ProtectedRouter";
import Formulario from "./components/Formulario";

export const ContextApp = createContext({});
localStorage.setItem("respCorrecta", 0);

const contenidoFormulario = [
  {
    pregunta: "la suma de 2 mas 2 es:",
    opciones: [
      {
        nombreOpcion: "4",
        esVerdadera: true
      },
      {
        nombreOpcion: "1",
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: "la multiplicacion de 2 * 2:",
    opciones: [
      {
        nombreOpcion: "4",
        esVerdadera: true
      },
      {
        nombreOpcion: "2",
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: "la division de 2 / 2 es:",
    opciones: [
      {
        nombreOpcion: "1",
        esVerdadera: true
      },
      {
        nombreOpcion: "3",
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: "la resta de 2 - 2 es:",
    opciones: [
      {
        nombreOpcion: "0",
        esVerdadera: true
      },
      {
        nombreOpcion: "4",
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: "2 a la pontencia de 2 es:",
    opciones: [
      {
        nombreOpcion: "4",
        esVerdadera: true
      },
      {
        nombreOpcion: "5",
        esVerdadera: false
      }
    ]
  }
];

const Resultado = () => {
  return (
    <div>
      <h3>Resumen</h3>
      {
        <label>
          {localStorage.getItem("respCorrecta") &&
            localStorage.getItem("respCorrecta")}{" "}
          Preguntas correctas{" "}
        </label>
      }
    </div>
  );
};

const App = () => {
  const sesion = ValidarLogeo();
  const [isLogin, setIsLogin] = useState(sesion);
  const [next, setNext] = useState(0);
  const [isEncuenta, setIsEncuenta] = useState(false);

  const onIsLogin = val => {
    setIsLogin(val);
  };
  const onNext = val => {
    setNext(val);
  };

  /*const onEncuesta = val => {
    setIsEncuenta(val);
    isEncuenta && alert("Encuesta Finalizo");
  };*/

  const onEncuesta = val => {
    setIsEncuenta(val);
  };

  return (
    <BrowserRouter>
      <ContextApp.Provider
        value={{ onIsLogin, isLogin, onEncuesta, isEncuenta }}
      >
        <Switch>
          <Route path="/resultado" component={Resultado} />
          <ProtectedRouter exact path="/">
            {
              <Container key={next} title={contenidoFormulario[next].pregunta}>
                <Formulario
                  id={next}
                  options={contenidoFormulario[next].opciones}
                  onChange={onNext}
                  len={contenidoFormulario.length}
                />
              </Container>
            }
          </ProtectedRouter>
          <Route path="/login" component={Login} />
        </Switch>
      </ContextApp.Provider>
    </BrowserRouter>
  );
};

export default App;
