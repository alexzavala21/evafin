import React from "react";

const Container = ({ title, children }) => {
  return (
    <div>
      <label>{title}</label>
      <div>{children}</div>
    </div>
  );
};

export default Container;
