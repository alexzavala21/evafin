import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import Select from "react-select";
import { Formik, Form, Field } from "formik";
import { ContextApp } from "../../App";

//const Formulario = ({ options }) => {
const Formulario = ({ id, options, onChange, len }) => {
  console.log(id);

  const data = options.map(opt => ({
    label: opt.nombreOpcion,
    value: opt.esVerdadera
  }));

  const contextApp = useContext(ContextApp);
  const [cont, setCont] = useState(0);
  const onSetCont = val => setCont(val);
  const history = useHistory();
  /*
  const [respuesta, setRespuesta] = useState(null);
  const [opcion, setOpcion] = useState(null);
  const [selected, setSelected] = useState(false);

  const handleChange = select => {
    var valor = null;
    data.forEach(opt => opt.label === select.label && (valor = opt.value));
    setRespuesta(valor);
    setSelected(false);
    setOpcion(select.label);
    setRespuesta(select.value);
  };
  */

  const SelectField = props => {
    return (
      <Select
        options={props.options}
        {...props.field}
        onChange={option => {
          props.form.setFieldValue(props.field.name, option);
          option.value && onSetCont(parseInt(cont) + 1);
        }}
      />
    );
  };

  //const onClick = () => opcion != null && setSelected(true);
  /*const onClick = e => {
    e.preventDefault();
    opcion != null && setSelected(true);
  };*/
  return (
    <div>
      <Formik
        initialValues={{ data, select: "" }}
        validate={values =>
          !values.select && alert("Debe seleccionar una opción")
        }
        //onSubmit={() => alert("Opción ya fue seleccionada")}
        onSubmit={() => {
          /*parseInt(id) + 1 !== len
            ? onChange(parseInt(id) + 1)
            : alert("Encuesta finalizada")
          parseInt(id) + 1 !== len
            ? onChange(parseInt(id) + 1)
            : contextApp.onEncuesta(true)*/

          if (parseInt(id) + 1 !== len) {
            onChange(parseInt(id) + 1);
            localStorage.setItem("respCorrecta", cont);
          } else {
            contextApp.onEncuesta(true);
            localStorage.setItem("respCorrecta", cont);
            history.push("/resultado");
          }
        }}
      >
        {({ values, handleSubmit }) => (
          <Form onSubmit={handleSubmit}>
            <Field
              name="select"
              options={values.data}
              component={SelectField}
            />
            {/*parseInt(id) + 1 === len ? (
              <button>Finalizar</button>
            ) : (
              <button>Siguiente</button>
            )*/
            parseInt(id) + 1 === len ? (
              <button>Finalizar</button>
            ) : (
              <button>Siguiente</button>
            )}
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Formulario;
