import React, { useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import { ContextApp } from "../../App";

export default ({ children, redirect = "/login", ...rest }) => {
  const contextApp = useContext(ContextApp);
  return (
    <Route {...rest}>
      {contextApp.isLogin ? children : <Redirect to={redirect} />}
    </Route>
  );
};
