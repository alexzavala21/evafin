import axios from "axios";

export const apilogin = async (user, callback) => {
  const res = await axios.post(
    "https://login-test-dga.herokuapp.com/login",
    user
  );

  if (res.data.response) {
    localStorage.setItem("username", user.username);
    localStorage.setItem("password", user.password);
    callback();
  }
};
