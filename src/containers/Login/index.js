import React, { useContext, useState } from "react";
import { useHistory, Redirect } from "react-router-dom";
//import axios from "axios";

import { ContextApp } from "../../App";
import { apilogin } from "../Utilidades/ApiLogeo";

import { Wrapper } from "./styles";
import { Label, Input } from "reactstrap";

export default () => {
  const history = useHistory();
  //const [isLogin, setIsLogin] = useState(false);
  const contextApp = useContext(ContextApp);
  const [username, setUser] = useState("");
  const [password, setPass] = useState("");
  /*const login = async (user, callback) => {
    const res = await axios.post(
      "https://login-test-dga.herokuapp.com/login",
      user
    );

    if (res.data.response) {
      setIsLogin(true);
      callback();
    }
   
  };*/

  const handleSubmit = e => {
    e.preventDefault();
    //login({ username, password }, () => history.push("/"));
    apilogin({ username, password }, () => {
      contextApp.onIsLogin(true);
      history.push("/");
    });
  };

  //if (isLogin) return <Redirect to="/" />;
  if (contextApp.isLogin) return <Redirect to="/" />;

  return (
    <Wrapper>
      <h1>Login</h1>

      <form onSubmit={handleSubmit}>
        <Label htmlFor="username">Usuario </Label>
        <Input
          id="username"
          type="text"
          name="username"
          value={username}
          onChange={e => setUser(e.target.value)}
        />

        <Label htmlFor="password">Contreseña </Label>
        <Input
          id="password"
          type="password"
          name="password"
          value={password}
          onChange={e => setPass(e.target.value)}
        />

        <button>Entrar</button>
      </form>
    </Wrapper>
  );
};
